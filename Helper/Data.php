<?php
/**
 * Pmclain_Twilio extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GPL v3 License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.gnu.org/licenses/gpl.txt
 *
 * @category       Pmclain
 * @package        Twilio
 * @copyright      Copyright (c) 2017
 * @license        https://www.gnu.org/licenses/gpl.txt GPL v3 License
 */

namespace Aalogics\Sms\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use \Aalogics\Sms\Logger\Logger;

class Data extends AbstractHelper
{
    const SMS_CONFIRM_PROCESSING = 'confirm_sms_processing';
    const SMS_CONFIRM_NEW = 'confirm_sms_pending';

    /**
     * @var \Aalogics\Sms\Logger\Logger
     */
    protected $_log;

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        \Aalogics\Sms\Logger\Logger $logger
    )
    {
        $this->_log = $logger;
        parent::__construct($context);
    }

    public function getStoreName()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnabled()
    {
        return $this->getAdminField('enable');
    }


    public function getSmsGateway()
    {
        $gateway = $this->getAdminField('sms_gateway');
        $this->debug('configuration gateway', $gateway);
        switch ($gateway) {
            case 'twilio':
                return \Aalogics\Sms\Model\Gateway\Twilio::class;
                break;
            case 'mobilink':
                return \Aalogics\Sms\Model\Gateway\Mobilink::class;
                break;
            case 'ufone':
                return \Aalogics\Sms\Model\Gateway\Ufone::class;
                break;
            case 'telenor':
                return \Aalogics\Sms\Model\Gateway\Telenor::class;
                break;
        }
        return FALSE;
    }

    /**
     *
     */
    public function getAdminField($key)
    {
        $value = $this->scopeConfig->getValue('aasms/general/' . $key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $value;
    }

    public function debug($message, $data = NULL)
    {
        if ($this->getAdminField('debug')) {
            $this->_log->debug($message . print_r($data, TRUE));
        }
    }

    public function smsDetails($order)
    {
        $randomCode = rand(11111, 99999);
        $this->debug('observer randomCode', $randomCode);

        $orderId = $order->getId();
        $this->debug('orderId', $orderId);
        $billingAddress = $order->getBillingAddress()->getData();

        //if verification is needed
        if ($this->getAdminField('sms_code_verification_status')) {
            $order->setData("sms_success_code", $randomCode);
            $order->save();
        }
        /* END Updating Order Table with verification Code */

        $storeName = $this->getStoreName();   
        $phone = $this->convertPhoneCode($billingAddress['telephone']);     
        // Creating message for customer
        $replaceKeywords = [
            '{first name}',
            '{last name}',
            '{order_increment_no}',
            '{store_name}',
            '{verification_code}'
        ];
        $replaceValues = [
            $billingAddress ['firstname'],
            $billingAddress ['lastname'],
            $order->getIncrementId(),
            $storeName,
            $randomCode
        ];
        $messageToSend = str_replace($replaceKeywords, $replaceValues, $this->getAdminField('sms_code_template'));
        $this->debug('messageToSend', $messageToSend);

        $smsData = [
            'firstname' => $billingAddress ['firstname'],
            'lastname' => $billingAddress ['lastname'],
            'phone' => $phone,
            'smsMessage' => $messageToSend,
            'orderIncId' => $order->getIncrementId(),
            'randomCode' => $randomCode,
            'storeName' => $storeName
        ];
        return $smsData;
    }

    public function smsUpdateOrderDetails($order, $comment = NULL)
    {
        $orderId = $order->getId();
        $this->debug('orderId', $orderId);
        $billingAddress = $order->getBillingAddress()->getData();
        //@todo fetch new status
        $status = $order->getStatus();
        $storeName = $this->getStoreName();        
        $phone = $this->convertPhoneCode($billingAddress['telephone']);
        $randomCode = FALSE;
        // Creating message for customer
        $replaceKeywords = [
            '{first name}',
            '{last name}',
            '{order_increment_no}',
            '{store_name}',
            '{order_status}',
            '{order_comment}'
        ];
        $replaceValues = [
            $billingAddress ['firstname'],
            $billingAddress ['lastname'],
            $order->getIncrementId(),
            $storeName,
            $status . '. ',
            $comment
        ];

        $messageToSend = str_replace($replaceKeywords, $replaceValues, $this->getAdminField('order_transactional_template'));
        $this->debug('messageToSend', $messageToSend);

        $smsData = [
            'firstname' => $billingAddress ['firstname'],
            'lastname' => $billingAddress ['lastname'],
            'phone' => $phone,
            'smsMessage' => $messageToSend,
            'orderIncId' => $order->getIncrementId(),
            'randomCode' => $randomCode,
            'storeName' => $storeName
        ];
        return $smsData;
    }

    public function smsConfirmOrderDetails($order, $comment = NULL)
    {
        $orderId = $order->getId();
        $this->debug('orderId', $orderId);
        $billingAddress = $order->getBillingAddress()->getData();
        //@todo fetch new status
        $status = $order->getStatus();
        $storeName = $this->getStoreName();        
        $phone = $this->convertPhoneCode($billingAddress['telephone']);
        $randomCode = FALSE;
        // Creating message for customer
        $replaceKeywords = [
            '{first name}',
            '{last name}',
            '{order_increment_no}',
            '{store_name}',
            '{order_status}',
            '{order_comment}'
        ];
        $replaceValues = [
            $billingAddress ['firstname'],
            $billingAddress ['lastname'],
            $order->getIncrementId(),
            $storeName,
            $status . '. ',
            $comment
        ];

        $messageToSend = str_replace($replaceKeywords, $replaceValues, $this->getAdminField('sms_two_way_template'));
        $this->debug('messageToSend', $messageToSend);

        $smsData = [
            'firstname' => $billingAddress ['firstname'],
            'lastname' => $billingAddress ['lastname'],
            'phone' => $phone,
            'smsMessage' => $messageToSend,
            'orderIncId' => $order->getIncrementId(),
            'randomCode' => $randomCode,
            'storeName' => $storeName
        ];
        return $smsData;
    }


    public function smsCancelOrderDetails($order, $comment = NULL)
    {
        $orderId = $order->getId();
        $this->debug('orderId', $orderId);
        $billingAddress = $order->getBillingAddress()->getData();
        //@todo fetch new status
        $status = $order->getStatus();
        $storeName = $this->getStoreName();        
        $phone = $this->convertPhoneCode($billingAddress['telephone']);
        $randomCode = FALSE;
        // Creating message for customer
        $replaceKeywords = [
            '{first name}',
            '{last name}',
            '{order_increment_no}',
            '{store_name}',
            '{order_status}',
            '{order_comment}'
        ];
        $replaceValues = [
            $billingAddress ['firstname'],
            $billingAddress ['lastname'],
            $order->getIncrementId(),
            $storeName,
            $status . '. ',
            $comment
        ];

        $messageToSend = str_replace($replaceKeywords, $replaceValues, $this->getAdminField('sms_cancel_template'));
        $this->debug('messageToSend', $messageToSend);

        $smsData = [
            'firstname' => $billingAddress ['firstname'],
            'lastname' => $billingAddress ['lastname'],
            'phone' => $phone,
            'smsMessage' => $messageToSend,
            'orderIncId' => $order->getIncrementId(),
            'randomCode' => $randomCode,
            'storeName' => $storeName
        ];
        return $smsData;
    }

    public function smsShippedOrderDetails($order, $comment = NULL)
    {
        $orderId = $order->getId();
        $this->debug('orderId', $orderId);
        $billingAddress = $order->getBillingAddress()->getData();
        //@todo fetch new status
        $status = $order->getStatus();
        $storeName = $this->getStoreName();
        $phone = $this->convertPhoneCode($billingAddress['telephone']);
        $randomCode = FALSE;
        // Creating message for customer
        $replaceKeywords = [
            '{first name}',
            '{last name}',
            '{order_increment_no}',
            '{store_name}',
            '{order_status}',
            '{order_comment}'
        ];
        $replaceValues = [
            $billingAddress ['firstname'],
            $billingAddress ['lastname'],
            $order->getIncrementId(),
            $storeName,
            $status . '. ',
            $comment
        ];

        $messageToSend = str_replace($replaceKeywords, $replaceValues, $this->getAdminField('sms_cancel_template'));
        $this->debug('messageToSend', $messageToSend);

        $smsData = [
            'firstname' => $billingAddress ['firstname'],
            'lastname' => $billingAddress ['lastname'],
            'phone' => $phone,
            'smsMessage' => $messageToSend,
            'orderIncId' => $order->getIncrementId(),
            'randomCode' => $randomCode,
            'storeName' => $storeName
        ];
        return $smsData;
    }

    public function convertPhoneCode($phone){
        // Replace 0092 with 92 only (Eliminate 00)
        $withNineTwo = strpos($phone, '0092');
        if($withNineTwo === 0){
            $phone = preg_replace('/0092/','92', $phone, 1);
        }
        
        // Replace 0092 with 92 only (Eliminate 00)
        $withTwo = strpos($phone, '00');
        if($withTwo === 0){
            $phone = preg_replace('/00/','92', $phone, 1);
        }
        
        // replace 0 from phone with 92
        $zeroPos = strpos($phone, '0');
        if( $zeroPos == 0 ){
            $phone = preg_replace('/0/','92', $phone, 1);
        }
        
        
        
        // adding + to phone 
        if($phone[0] != '+'){
            $phone = '+'.$phone;
        }
        return $phone;
    }

    


}
