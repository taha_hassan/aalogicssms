<?php

namespace Aalogics\Sms\Logger;

use Magento\Framework\Logger\Handler\Base;

class Handler extends Base
{
    /**
     * @var string
     */
    protected $fileName = '/var/log/aasms.log';

    /**
     * @var int
     */
    protected $loggerType = Logger::DEBUG;
}