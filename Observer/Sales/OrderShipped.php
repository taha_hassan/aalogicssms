<?php
/**
 * Pmclain_Twilio extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GPL v3 License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.gnu.org/licenses/gpl.txt
 *
 * @category       Pmclain
 * @package        Twilio
 * @copyright      Copyright (c) 2017
 * @license        https://www.gnu.org/licenses/gpl.txt GPL v3 License
 */

namespace Aalogics\Sms\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Aalogics\Sms\Helper\Data as Helper;
use \Aalogics\Sms\Model\GatewayFactory;

class OrderShipped implements ObserverInterface
{
    /**
     * @var \Pmclain\Twilio\Helper\Data
     */
    protected $_helper;

    protected $logger;

    protected $order;

    protected $_gateWayFactory;

    public function __construct(
        Helper $helper,
    	\Aalogics\Sms\Model\GatewayFactory $gateWayFactory,
        \Aalogics\Sms\Logger\Logger $logger,
        \Magento\Sales\Model\Order $order
    ) {
    	$this->_gateWayFactory = $gateWayFactory;
        $this->_helper = $helper;
        $this->logger = $logger;
        $this->order = $order;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Magento\Framework\Event\Observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try{
            	$this->_helper->debug('execute observer sales order shipped');
            	if($this->_helper->isEnabled() 
            		&& $this->_helper->getAdminField('sms_shipped_status')
        			&& $gateWay = $this->_helper->getSmsGateway()
        		) {
            		$gateWayObj = $this->_gateWayFactory->create($gateWay);
                    $shipment = $observer->getEvent()->getShipment();
                    $order = $shipment->getOrder();
        	        if ($order->getBillingAddress()->getTelephone()) {
         	            $gateWayObj->sendOrderSms($this->_helper->smsShippedOrderDetails($order));
        	        }
            	}
            }catch(\Exception $e ){
                $exception = array('exception' => $e->getMessage());
               $this->logger->debug("Exception", $exception);         
            }
        return $observer;
    }
    
    
}
