<?php
namespace Aalogics\Sms\Block\Adminhtml\Order;

use Magento\Sales\Block\Adminhtml\Order\View as OrderView;

class View
{
    public function beforeSetLayout(OrderView $subject)
    {
        $message ='Are you sure you want to send sms?';
        $url = $subject->getBaseUrl().'aasms/index/confirmdetails?id=' . $subject->getOrder()->getRealOrderId();

        $subject->addButton(
            'order_custom_button',
            [
                'label' => __('Send Order SMS'),
                'class' => __('send-order-sms'),
                'id' => 'order-view-sms-button',
                'onclick' => "confirmSetLocation('{$message}', '{$url}')"
            ]
        );
    }
}