<?php
namespace Aalogics\Sms\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Integration\Model\ConfigBasedIntegrationManager;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Sales\Model\Order\Status;

class UpgradeData implements UpgradeDataInterface
{
	protected $orderStatus;

	/**
	 * @param ConfigBasedIntegrationManager $integrationManager
	 */

	public function __construct(Status $orderStatus)
	{
		$this->orderStatus = $orderStatus;  
	}

	/**
	 * {@inheritdoc}
	 */

	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		if (version_compare($context->getVersion(), '1.0.1', '<')) {
			$setup->startSetup();
			
			$data['status']='confirm_sms_pending'; // status code  must be unique
			$data['label']= __('SMS CONFIRM'); // status label
			
			$setup->getConnection()
			->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], array($data));
			
			$setup->getConnection()
			->insertArray($setup->getTable('sales_order_status_state'), ['status', 'state'], 
					array(['status' => $data['status'], 'state' => \Magento\Sales\Model\Order::STATE_NEW]));
			
			$setup->endSetup();
		}

		if (version_compare($context->getVersion(), '1.0.2', '<')) {
			$setup->startSetup();

			$data['status']	='sms_delivery_confirm'; // status code  must be unique
			$data['label']	= __('SMS DELIVERY CONFIRM'); // status label
			
			$setup->getConnection()
			->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], array($data));
			
			$setup->getConnection()
			->insertArray($setup->getTable('sales_order_status_state'), ['status', 'state'], 
					array(['status' => $data['status'], 'state' => \Magento\Sales\Model\Order::STATE_PROCESSING]));


			
			$data['status']	='sms_delivery_pending'; // status code  must be unique
			$data['label']	= __('SMS DELIVERY PENDING'); // status label



			$setup->getConnection()
			->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], array($data));
			
			$setup->getConnection()
			->insertArray($setup->getTable('sales_order_status_state'), ['status', 'state'], 
					array(['status' => $data['status'], 'state' => \Magento\Sales\Model\Order::STATE_PROCESSING]));


			
			$setup->endSetup();
		}
// 		$this->integrationManager->processIntegrationConfig(['TestIntegration']);
	}
}