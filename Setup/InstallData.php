<?php
namespace Aalogics\Sms\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Integration\Model\ConfigBasedIntegrationManager;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Sales\Model\Order\Status;

class InstallData implements InstallDataInterface
{
	protected $orderStatus;

	/**
	 * @param ConfigBasedIntegrationManager $integrationManager
	 */

	public function __construct(Status $orderStatus)
	{
		$this->orderStatus = $orderStatus;  
	}

	/**
	 * {@inheritdoc}
	 */

	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();
		
		$data['status']='confirm_sms_processing'; // status code  must be unique
		$data['label']= __('SMS CONFIRM'); // status label
		
		$setup->getConnection()
		->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], array($data));
		
		$setup->getConnection()
		->insertArray($setup->getTable('sales_order_status_state'), ['status', 'state'], 
				array(['status' => $data['status'], 'state' => \Magento\Sales\Model\Order::STATE_PROCESSING]));
		
		$setup->endSetup();
// 		$this->integrationManager->processIntegrationConfig(['TestIntegration']);
	}
}