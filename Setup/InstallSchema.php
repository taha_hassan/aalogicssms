<?php

/**
* Copyright 2018 AALOGICS. All rights reserved.
* See LICENSE.txt for license details.
*/
namespace Aalogics\Sms\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * 
 * @package Aalogics\Sms\Setup
 */
class InstallSchema implements InstallSchemaInterface {
	protected $status;
	public function __construct(\Magento\Sales\Model\Order\Status $status) {
		$this->status = $status;
	}
	
	/**
	 *
	 * @ERROR!!! @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 * @throws \Zend_Db_Exception
	 */
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
		$installer = $setup;
		$installer->startSetup ();
		$tableName = $setup->getTable ( 'sales_order' );
		
		$columns = [
		'twilio_sms_id' => [
		'length' => '255',
		'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		'nullable' => TRUE,
		'comment' => 'Twilio Sms Id',
		],
		'sms_success_code' => [
		'length' => '255',
		'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		'nullable' => TRUE,
		'comment' => 'Success code for verification through SMS',
		],
		
		];
		
		$connection = $installer->getConnection();
		foreach ($columns as $name => $definition) {
			$connection->addColumn($tableName, $name, $definition);
		}
		
		$installer->endSetup ();
	}
}
