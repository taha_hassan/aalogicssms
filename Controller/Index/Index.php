<?php
/**
 * Copyright � Aalogics Ltd. All rights reserved.
 *
 * @package    Aalogics_Sms
 * @copyright  Copyright � Aalogics Ltd (http://www.aalogics.com)
 */

namespace Aalogics\Sms\Controller\Index;

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        $confirmArray = array(
            'yes',
            'Yes',
            'YES',
            'y',
            'Y'
        );

        $this->getResponse()->representJson($this->jsonHelper->jsonEncode(array('message' => 'TODO')));
    }

}