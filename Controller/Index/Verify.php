<?php
/**
 * Copyright © Aalogics, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Aalogics\Sms\Controller\Index;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use \Aalogics\Sms\Helper\Data;
use \Magento\Sales\Api\Data\OrderInterface;
use \Magento\Sales\Mode\Order;

class Verify extends \Magento\Framework\App\Action\Action
{
    /**
     *
     * @var \Aalogics\Sms\Helper\Data
     */
    protected $helper;

    protected $order;

    /**
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Sales\Api\OrderCustomerManagementInterface $orderCustomerService
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Aalogics\Sms\Helper\Data $helper,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Framework\Json\Helper\Data $jsonData,
        \Magento\Sales\Model\Order $orderModel
    )
    {
        $this->helper = $helper;
        $this->order = $order;
        $this->jsonHelper = $jsonData;
        parent::__construct($context);
    }

    /**
     * Execute request
     *
     * @return \Magento\Framework\Controller\Result\Json
     * @throws NoSuchEntityException
     * @throws \Exception
     * @throws AlreadyExistsException
     */
    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        foreach ($postData['verification_code'] as $check) {
            $data[] = $check;

        }
        $verificationCode = implode("", $data);
        if ($this->validatedParams($postData)) {
            $order_id = $postData['order_id'];
            $orderObj = $this->order->loadByIncrementId($order_id);
            if ($orderObj) {
                $verificationCodeDb = $orderObj->getSmsSuccessCode();
                if ($verificationCode == $verificationCodeDb

                ) {
                    if ($orderObj->getStatus() != \Aalogics\Sms\Helper\Data::SMS_CONFIRM_NEW
                        || $orderObj->getStatus() != \Aalogics\Sms\Helper\Data::SMS_CONFIRM_PROCESSING) {

                        if (
                            $orderObj->getState() == \Magento\Sales\Model\Order::STATE_NEW
                            || $orderObj->getState() == \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT

                        ) {
                            $orderStatus = \Aalogics\Sms\Helper\Data::SMS_CONFIRM_NEW;
                            $orderObj->setStatus($orderStatus);
                            $orderObj->save();
                        } elseif ($orderObj->getState() == \Magento\Sales\Model\Order::STATE_PROCESSING) {
                            $orderStatus = \Aalogics\Sms\Helper\Data::SMS_CONFIRM_PROCESSING;
                            $orderObj->setStatus($orderStatus);
                            $orderObj->save();
                        } else {
                            $response = array(
                                'status' => false,
                                'msg' => __('We can not process this order , please contact support'),
                                'classes' => 'error-msg'
                            );
                        }
                        $response = array(
                            'status' => true,
                            'msg' => __('Your order is confirmed and will be processed shortly!'),
                            'classes' => 'success-msg'
                        );
                    } else {
                        $response = array(
                            'status' => false,
                            'msg' => __('Order is already verified.'),
                            'classes' => 'success-msg'
                        );
                    }

                } else {
                    $response = array(
                        'status' => false,
                        'msg' => __('Please try with a correct verification Code.'),
                        'classes' => 'error-msg'
                    );

                }
            }
        } else {
            $response = array(
                'status' => false,
                'msg' => __('Required Fields are not filled.')
            );
        }


        $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function validatedParams()
    {
        $request = $this->getRequest();
        if (trim($request->getParam('order_id')) === '') {
            throw new LocalizedException(__('Order Id is missing'));
        }
        return $request->getParams();
    }
}