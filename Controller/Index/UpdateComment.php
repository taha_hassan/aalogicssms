<?php
/**
 * Copyright © Aalogics, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Aalogics\Sms\Controller\Index;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use \Aalogics\Sms\Helper\Data;
use \Magento\Sales\Api\Data\OrderInterface;
use \Magento\Sales\Model\Order;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;


class UpdateComment extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{
    /**
     *
     * @var \Aalogics\Sms\Helper\Data
     */
    protected $helper;

    protected $order;

    /**
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Sales\Api\OrderCustomerManagementInterface $orderCustomerService
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Aalogics\Sms\Helper\Data $helper,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Framework\Json\Helper\Data $jsonData,
        \Magento\Sales\Model\Order $orderModel
    )
    {
        $this->helper = $helper;
        $this->order = $order;
        $this->jsonHelper = $jsonData;
        parent::__construct($context);
    }

    /**
     * Execute request
     *
     * @return \Magento\Framework\Controller\Result\Json
     * @throws NoSuchEntityException
     * @throws \Exception
     * @throws AlreadyExistsException
     */
    public function execute()
    {
        try{
            $postData = $this->getRequest()->getPostValue();
            $From = $postData['From'];
            $Body = explode(' ', $postData['Body'], 3);

                $order_id   = $Body['0'];
                $status     = $Body['1'];
                $comment    = $Body['2'];


               
                $orderObj = $this->order->loadByIncrementId($order_id);
                if ($orderObj) {
                        $confirmArray = array(
                            'yes',
                            'Yes',
                            'YES',
                            'y',
                            'Y'
                        );

                        if(in_array($status,$confirmArray))
                        {
                            $status = 'sms_delivery_confirm';
                        }
                        else
                        {
                            $status = 'sms_delivery_pending';
                        }

                    $billingAddress = $orderObj->getBillingAddress();                
                    $phone = $this->helper->convertPhoneCode($billingAddress['telephone']);
                    if ($From == $phone) {
                                $orderObj->addStatusHistoryComment($comment,$status);
                                $orderObj->save();
                    }
                }
            }
            catch ( \Exception $e ) {
            throw $e;
        }
    }


    public function createCsrfValidationException(RequestInterface $request): ? InvalidRequestException
    {
        return null;
    }
        
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}