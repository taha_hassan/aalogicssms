<?php
/**
 * Copyright © Aalogics, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Aalogics\Sms\Controller\Index;


use Magento\Framework\App\Action\Context;
use Aalogics\Sms\Helper\Data as Helper;
use \Aalogics\Sms\Model\GatewayFactory;
use Magento\Framework\Controller\ResultFactory; 



class ConfirmDetails extends \Magento\Framework\App\Action\Action
{
	protected $order;
    protected $_helper;
    protected $logger;
    protected $_gateWayFactory;
    protected $resultRedirect;
    protected $_backendUrl;



	public function __construct(
        Helper $helper,
        \Magento\Sales\Api\Data\OrderInterface $order,
    	\Aalogics\Sms\Model\GatewayFactory $gateWayFactory,
        \Aalogics\Sms\Logger\Logger $logger,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\ResultFactory $result,
        \Magento\Store\Model\StoreManagerInterface $manStore,
        \Magento\Backend\Model\UrlInterface $backendUrl
    )
    {
        $this->order = $order;
        $this->_helper = $helper;
        $this->logger = $logger;
    	$this->_gateWayFactory = $gateWayFactory;
   		$this->resultRedirect = $result;
        $this->manStore = $manStore;
        $this->_backendUrl = $backendUrl;
        parent::__construct($context);
    }


    public function execute()
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $store = $this->manStore->getStore();
    	try{
            	$this->_helper->debug('execute controller send custom message');
            	if($this->_helper->isEnabled() 
                    && $this->_helper->getAdminField('sms_two_way_status')
                    && $gateWay = $this->_helper->getSmsGateway()
                ) {
            		$this->_helper->debug('Gateway',$gateWay);
            		$gateWayObj = $this->_gateWayFactory->create($gateWay);
			    	$params = $this->_request->getParams('id');
			    	$order = $this->order->loadByIncrementId($params);
			    	if (!$order->getId()) {             
                            $url = $this->_backendUrl->getUrl('sales/order/view/'); // second arg can be omitted
        		            $resultRedirect->setPath($url);
        			}   
			    	$comment = '';   
        	        if ($order->getBillingAddress()->getTelephone()) {
        	        	$this->_helper->debug('SMS'.print_r($this->_helper->smsConfirmOrderDetails($order, $comment),TRUE));
         	            $gateWayObj->sendOrderSms($this->_helper->smsConfirmOrderDetails($order, $comment));
                        $url = $this->_backendUrl->getUrl('sales/order/view/',['order_id' => $order->getId()]); // second arg can be omitted
                        $resultRedirect->setPath($url);

        	        }
            	}
            	else {
            		$this->_helper->debug('Else Execute');    
                    $url = $this->_backendUrl->getUrl('sales/*'); // second arg can be omitted
                    $resultRedirect->setPath($url);
            	}
            }catch(\Exception $e ){
                $exception = array('exception' => $e->getMessage());
               $this->logger->debug("Exception", $exception);         
            }
            return $resultRedirect;
    }


}