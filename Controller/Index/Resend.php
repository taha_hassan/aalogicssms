<?php
/**
 * Copyright © Aalogics Ltd. All rights reserved.
 *
 * @package    Aalogics_Sms
 * @copyright  Copyright © Aalogics Ltd (http://www.aalogics.com)
 */

namespace Aalogics\Sms\Controller\Index;

use Aalogics\Sms\Helper\Data as Helper;
use \Aalogics\Sms\Model\GatewayFactory;


class Resend extends \Magento\Framework\App\Action\Action
{
    protected $_helper;

    protected $logger;

    protected $_gateWayFactory;

    protected $_checkoutSession;

    public function __construct(
        Helper $helper,
        \Aalogics\Sms\Model\GatewayFactory $gateWayFactory,
        \Aalogics\Sms\Logger\Logger $logger,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_gateWayFactory = $gateWayFactory;
        $this->_helper = $helper;
        $this->logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_pageFactory = $pageFactory;

        return parent::__construct($context);
    }

    public function execute()
    {

        $this->_helper->debug('execute observer sales order after');
        if ($this->_helper->isEnabled()
            && $this->_helper->getAdminField('sms_code_verification_status')
            && $gateWay = $this->_helper->getSmsGateway()
        ) {
            $this->_helper->debug('Gateway', $gateWay);
            $gateWayObj = $this->_gateWayFactory->create($gateWay);
            $order = $this->_checkoutSession->getLastRealOrder();
            if ($order->getBillingAddress()->getTelephone()) {
                $gateWayObj->sendOrderSms($this->_helper->smsDetails($order));


            }
        }

    }

}
