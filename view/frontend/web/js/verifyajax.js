
define([
    "jquery",
    "jquery/ui"
], function($){
    "use strict";
     
    function main(config, element) {
        var $element = $(element);
        var AjaxUrl = config.AjaxUrl;
         
        var dataForm = $('#verify-sms');
        dataForm.mage('validation', {});
         
        $(document).on('click','.submit',function() {
             
            if (dataForm.valid()){
            event.preventDefault();
                var param = dataForm.serialize();
                    $.ajax({
                        showLoader: true,
                        url: AjaxUrl,
                        data: param,
                        type: "POST"
                    }).done(function (data) {
                        $( ".note" ).empty();
                        if(data.status){
                            $('#verify-sms').html('<div class="alert alert-success"><strong>Success! </strong>'+data.msg+'.</div>');
                        }
                        else{
                            $('.note').html('<div class="alert alert-warning fade in alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+data.msg+'.</div>');
                        }
                        document.getElementById("verify-sms").reset();
                        return true;
                    });
                }
            });
    };
return main;
     
     
});