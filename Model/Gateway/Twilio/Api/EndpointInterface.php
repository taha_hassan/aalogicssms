<?php
namespace Aalogics\Sms\Model\Gateway\Twilio\Api;

interface EndpointInterface {
	
	public function makeRequestParams($parameters = []);
	
	public function makeRequestHeaders($parameters = []);
}