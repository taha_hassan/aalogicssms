<?php
namespace Aalogics\Sms\Model\Gateway\Twilio\Api;
use \Magento\Framework\Model\AbstractModel;
use \Aalogics\Sms\Model\Gateway\Twilio\Api\Client;
use \Aalogics\Sms\Helper\Data;
use \Magento\Framework\DataObject;
use \Magento\Framework\HTTP\ZendClient;
use Magento\Framework\Encryption\EncryptorInterface;

class Request extends DataObject {
	
	/**
	 * @var EncryptorInterface
	 */
	protected $encryptor;
	
	
	/**
	 * HTTP Client
	 * @var client
	 */
	protected $client;
	
	/**
	 * Helper
	 * @var \Aalogics\Sms\Helper\Data
	 */
	protected $helper;
	
	/**
	 * 
	 * @param \Aalogics\Sms\Model\Gateway\Twilio\Api\Client $client
	 * @param \Aalogics\Sms\Logger\Logger $logger
	 */
	public function __construct(
		\Aalogics\Sms\Model\Gateway\Twilio\Api\Client $client,
		\Aalogics\Sms\Helper\Data $helper,
		EncryptorInterface $encryptor
	) {
		$this->client = $client;
		$this->helper = $helper;
		$this->encryptor  = $encryptor;
	}	
	
	/**
	 * 
	 * @return boolean|\Magento\Customer\Model\Data\Customer
	 */
	public function sendSms($messageText,$toNumbersCsv) {
		if (!$products = $this->_sendSms ($messageText, $toNumbersCsv)) {
			return false;
		}
	
		return $products;
	}
	
	
	/**
	 * Connect to Sms and sync products.
	 *
	 * @throws Exception
	 */
	protected function _sendSms($messageText, $toNumbersCsv) {
		try {
					$username = $this->helper->getAdminField( 'account_sid_twilio');	
					$password = $this->helper->getAdminField( 'auth_token_twilio_twilio');
					$from = $this->helper->getAdminField( 'sender_number_twilio');
				$response = $this->client->makeRequest(
						\Aalogics\Sms\Model\Gateway\Twilio\Api\Endpoints\Sendsms::class,
						ZendClient::POST,
						$this->_makeRequestArray('send_sms',
								[
								'Username' 	=> $username,
								'Password' 	=> $password,
								'To' 		=> $toNumbersCsv,
								'From' 		=> $from,
								'Body' 		=> $messageText
								]
					)
				);
					//$this->helper->debug('SampleResponse',$response);
				/*if($response['error_message'] == null) {
					return TRUE;
				}else {
					$this->helper->debug('Response',$response);
					throw new \Exception ( 'Unable to deliver message' );
				}*/
		} catch ( \Exception $e ) {
			throw $e;
		}
	
	}
	
	
	/**
	 * 
	 * @param unknown $type
	 * @param unknown $parameters
	 * @return multitype:NULL string unknown Ambigous <string, unknown>
	 */
	protected function _makeRequestArray($type, $parameters = []) {
		$result = [];
		
		switch ($type) {
			case 'send_sms':
				$result =array(
						'To' 	=> $parameters['To'],
						'Body' 	=> $parameters['Body'],
						'From' 	=> $this->helper->getAdminField('sender_number_twilio'),
						'Username' => $this->helper->getAdminField('account_sid_twilio'),
						'Password' => $this->helper->getAdminField('auth_token_twilio_twilio')
				);
				break;
		}
		return $result;
	}
}