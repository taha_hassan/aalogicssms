<?php
namespace Aalogics\Sms\Model\Gateway\Twilio\Api;

use \Magento\Framework\HTTP\ZendClientFactory;
use \Aalogics\Sms\Model\Gateway\Twilio\Api\Endpoints\Connect;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Aalogics\Sms\Logger\Logger;
use \Magento\Framework\HTTP\ZendClient;
use \Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use \Magento\Framework\Xml\Parser;
use \Aalogics\Sms\Helper\Data as SmsHelper;

class Client extends \Magento\Framework\DataObject {
	
	/**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfigObject;

    /**
     * 
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    protected $scopeConfigWriter;

    /**
     * @var \Aalogics\Sms\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory $clientFactory
     */
    protected $clientFactory;
    
    /**
     * 
     * @var \Aalogics\Sms\Model\Api\EndpointFactory
     */
    protected $endpointFactory;
    
     /**
      * @var \Aalogics\Sms\Helper\Data
      */
     protected $_helper;

     /**
      * @var \Aalogics\Sms\Helper\Data
      */
     protected $curlClient;  

    /**
     * 
     * @var \Magento\Framework\Xml\Parser
     */
    protected $xmlHelper;
	

	protected $jsonHelper;


	public function __construct(
			\Magento\Framework\Json\Helper\Data $jsonHelper,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Aalogics\Sms\Logger\Logger $logger,
			\Magento\Framework\HTTP\ZendClientFactory $clientFactory,
			EndpointFactory $endpointFactory,
			ConfigInterface $scopeWriter,
			\Magento\Framework\HTTP\Client\Curl $curl,
			\Aalogics\Sms\Helper\Data $SmsHelper,
			\Magento\Framework\Xml\Parser $xmlHelper,
			array $data = []
	) {
			$this->xmlHelper = $xmlHelper;
			$this->jsonHelper = $jsonHelper;
			$this->_helper = $SmsHelper;
			$this->scopeConfigWriter = $scopeWriter;
			$this->scopeConfigObject = $scopeConfig;
			$this->logger = $logger;
			$this->curlClient = $curl;
			$this->clientFactory = $clientFactory;		
			$this->endpointFactory = $endpointFactory;
			parent::__construct($data);
	}
	
	public function makeRequest($endPoint ,$method, $params , $headers = array()) {

		$url = $this->_helper->getAdminField( 'twilio_api_url' )."/2010-04-01/Accounts/".$params['Username']."/Messages.json";
		$this->_helper->debug('Request URL',$url);
		//https://api.twilio.com/2010-04-01/Accounts/ACcc8be3e6f20c22580457325c32e0c33a/Messages.json
		$endPointObj = $this->endpointFactory->create($endPoint);

		$url = $url.$endPointObj->getData('endpoint');
		
		/* $headers = $endPointObj->makeRequestHeaders(array(
				'access_token' => $this->getData('access_token'),
				'source_identifier' => $this->scopeConfigObject->getValue('aaSms/synnexb2b/api_source_identifier')
		)); */
		
		/** @var \Magento\Framework\HTTP\ZendClient $client */

		$client = $this->clientFactory->create();

		$clientConfig = ['verifypeer' => FALSE];
		$client->setConfig($clientConfig);

		switch ($method) {
			case ZendClient::POST:
				$parameters = $endPointObj->makeRequestParams($params);
				$this->_helper->debug('Request Params',$this->jsonHelper->jsonEncode($parameters));
				$client->setParameterPost($this->jsonHelper->jsonEncode($parameters), 'application/json');
				if($endPointObj instanceof \Aalogics\Sms\Model\Api\Endpoints\TransactionDetails) {
					$url = $url.'/'.$params['id'];
				}
			break;
			case ZendClient::PUT:
				$parameters = $endPointObj->makeRequestParams($params);
				$this->_helper->debug('Request Params',$this->jsonHelper->jsonEncode($parameters));
				if($endPointObj instanceof \Aalogics\Sms\Model\Api\Endpoints\TransactionStatus) {
					$url = $url.'/'.$params['name'];
				}else {
					$url = $url.'/'.$params['id'];
				}
				$client->setRawData($this->jsonHelper->jsonEncode($parameters), 'application/json');
			break;
			case ZendClient::GET:
			case ZendClient::DELETE:
				$parameters = $endPointObj->makeRequestParams($params);
				$client->setParameterGet($parameters);
				
			break;
		}
		$this->_helper->debug('Request URL',$url);
// 		$this->_helper->debug('Request Headers',$headers);
		$this->_helper->debug('Request Method',$method);
		$this->_helper->debug('Request Params',$parameters);
		$client->setUri($url);
		$client->setMethod($method);
		$client->setParameterPost(array('Body'=>$params['Body']));
		$client->setParameterPost(array('From'=>$params['From']));
		$client->setParameterPost(array('To'=>$params['To']));
		$client->setAuth($params['Username'],$params['Password']);
// 		$client->setHeaders($headers);
		$client->setHeaders('Accept','application/json');
		$client->setHeaders(\Zend_Http_Client::CONTENT_TYPE, 'application/json');
			$response = $client->request();
		/*try {
			$response = $client->request();
			//$this->_helper->debug('Client Response',$response);
		} catch (\Exception $e) {
			$this->_helper->debug('Exception : ',$e->getMessage());
			$this->scopeConfigWriter->saveConfig('aasms/general/twilio_token', FALSE,'default', 0);
			throw new \Exception ( 'Exception : '.$e->getMessage() );
		}
		if (($response->getStatus() < 200 || $response->getStatus() > 210)) {
			//$this->_helper->debug('Deployment marker request did not send a 200 status code.');
			throw new \Exception ( 'Deployment marker request did not send a 200 status code' );
		}
		$response = $this->jsonHelper->jsonEncode($response->getBody());*/
		return $response;
	}
}
