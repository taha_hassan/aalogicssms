<?php
namespace Aalogics\Sms\Model\Gateway\Ufone\Api;

interface EndpointInterface {
	
	public function makeRequestParams($parameters = []);
	
	public function makeRequestHeaders($parameters = []);
}