<?php

namespace Aalogics\Sms\Model\Gateway;

use \Magento\Framework\DataObject;
use \Aalogics\Sms\Logger\Logger;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Aalogics\Sms\Model\GatewayInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use  \Aalogics\Sms\Model\Gateway\Telenor\Api\Request;

class Telenor extends AbstractGateway implements GatewayInterface
{

    protected $_name = 'telenor';


    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $_storeManager;

    /**
     * @var \Aalogics\Sms\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     *
     */
    protected $scopeConfig;

    /**
     * @var \Aalogics\Sms\Helper\Data
     */
    protected $_helper;

    /**
     *
     * @var \Aalogics\Sms\Model\Gateway\Telenor\Api\Request
     */
    protected $_telenorRequest;

    const LOG_FILE = 'aasms.log';
    const LOG_TOKEN_FILE = 'aasms_token.log';
    const GATEWAY_NAME = 'telenor';

    /**
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Module\Dir\Reader $dirReader
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $storeConfig
     */
    public function __construct(
        \Magento\Store\Model\StoreManager $storeManager,
        \Aalogics\Sms\Logger\Logger $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Aalogics\Sms\Helper\Data $helper,
        \Aalogics\Sms\Model\Gateway\Telenor\Api\Request $telenorRequest
    )
    {
        $this->_telenorRequest = $telenorRequest;
        $this->_helper = $helper;
        $this->_logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $data['name'] = $this->_name;
        parent::__construct($data, $storeManager, $logger, $scopeConfig, $helper);
    }

    /*
     * Send sms to customer using telenor sms service
    */
    public function sendOrderSms($parameters = [])
    {

        $phone = $this->parsePhone($parameters['phone']);
        $result = $this->_telenorRequest->sendSms($parameters['smsMessage'], $phone);
    }
}