<?php

namespace Aalogics\Sms\Model\Gateway;

use \Magento\Framework\DataObject;
use \Aalogics\Sms\Logger\Logger;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Aalogics\Sms\Model\GatewayInterface;
use \Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Store\Model\StoreManager;

abstract class AbstractGateway extends DataObject implements GatewayInterface
{

    protected $_name = '';


    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $_storeManager;

    /**
     * @var \Aalogics\Sms\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     *
     */
    protected $scopeConfig;

    /**
     * @var \Aalogics\Sms\Helper\Data
     */
    protected $_helper;


    /**
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Module\Dir\Reader $dirReader
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $storeConfig
     */
    public function __construct(
        array $data = [],
        \Magento\Store\Model\StoreManager $storeManager,
        \Aalogics\Sms\Logger\Logger $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Aalogics\Sms\Helper\Data $helper
    )
    {
        $data['name'] = $this->_name;
        parent::__construct($data);
    }

    public function parsePhone($phone)
    {
        // replace + from phone
        $phone = str_replace('+', '', $phone);

        // Replace 0092 with 92 only (Eliminate 00)
        $withNineTwo = strpos($phone, '0092');
        if ($withNineTwo === 0) {
            $phone = preg_replace('/00/', '', $phone, 1);
        }

        // replace 0 from phone with 92
        $zeroPos = strpos($phone, '0');
        if ($zeroPos == 0) {
            $phone = preg_replace('/0/', '92', $phone, 1);
        }

        return $phone;
    }
}