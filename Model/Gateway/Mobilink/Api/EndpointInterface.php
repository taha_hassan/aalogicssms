<?php
namespace Aalogics\Sms\Model\Gateway\Mobilink\Api;

interface EndpointInterface {
	
	public function makeRequestParams($parameters = []);
	
	public function makeRequestHeaders($parameters = []);
}