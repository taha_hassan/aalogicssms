<?php
namespace Aalogics\Sms\Model\Gateway\Mobilink\Api;
use \Magento\Framework\Model\AbstractModel;
use \Aalogics\Sms\Model\Gateway\Mobilink\Api\Client;
use \Aalogics\Sms\Helper\Data;
use \Magento\Framework\DataObject;
use \Magento\Framework\HTTP\ZendClient;
use Magento\Framework\Encryption\EncryptorInterface;

class Request extends DataObject {
	
	/**
	 * @var EncryptorInterface
	 */
	protected $encryptor;
	
	/**
	 * HTTP Client
	 * @var client
	 */
	protected $client;
	
	/**
	 * Helper
	 * @var \Aalogics\Sms\Helper\Data
	 */
	protected $helper;
	
	/**
	 * 
	 * @param \Aalogics\Sms\Model\Gateway\Mobilink\Api\Client $client
	 * @param \Aalogics\Sms\Logger\Logger $logger
	 */
	public function __construct(
		\Aalogics\Sms\Model\Gateway\Mobilink\Api\Client $client,
		\Aalogics\Sms\Helper\Data $helper,
		EncryptorInterface $encryptor
			
	) {
		$this->client = $client;
		$this->helper = $helper;
		$this->encryptor  = $encryptor;
	}	
	
	/**
	 * 
	 * @return boolean|\Magento\Customer\Model\Data\Customer
	 */
	public function sendSms($messageText,$toNumbersCsv) {
		if (!$products = $this->_sendSms ($messageText, $toNumbersCsv)) {
			return false;
		}
	
		return $products;
	}
	
	
	/**
	 * Connect to Sms and sync products.
	 *
	 * @throws Exception
	 */
	protected function _sendSms($messageText, $toNumbersCsv) {
		try {
			if($this->client->getData('mobilink_auth_enable') == true){
			$this->client->connect ();
			if ($access_token = $this->client->getData('access_token')) {
				$this->helper->debug('sendsms start');
				//check if customer exists , if exists send update request else create request
				$response = $this->client->makeRequest(
						\Aalogics\Sms\Model\Gateway\Mobilink\Api\Endpoints\Sendsms::class,
						ZendClient::GET,
						$this->_makeRequestArray('send_sms',
								[
								'access_token' => $access_token,
								'to_number' => $toNumbersCsv,
								'sms_text' => $messageText
								]
					)
				);
				if($response['corpsms']['response'] == 'OK') {
					return TRUE;
				}else {
					$this->helper->debug('Response',$response);
					throw new \Exception ( 'Unable to deliver message' );
				}
			} 
		}else {
				$this->helper->debug('sendsms start');
				//check if customer exists , if exists send update request else create request
				$response = $this->client->makeRequest(
						\Aalogics\Sms\Model\Gateway\Mobilink\Api\Endpoints\Sendsms::class,
						ZendClient::GET,
						$this->_makeRequestArray('send_sms',
								[
								'to_number' => $toNumbersCsv,
								'sms_text' => $messageText
								]
					)
				);
				if($response['corpsms']['response'] == 'OK') {
					return TRUE;
				}else {
					$this->helper->debug('Response',$response);
					throw new \Exception ( 'Unable to deliver message' );
				}
			}
		} catch ( \Exception $e ) {
			throw $e;
		}
	
	}
	
	
	/**
	 * 
	 * @param unknown $type
	 * @param unknown $parameters
	 * @return multitype:NULL string unknown Ambigous <string, unknown>
	 */
	protected function _makeRequestArray($type, $parameters = []) {
		$result = [];
		
		switch ($type) {
			case 'send_sms':
				$result =array(
						'To' => $parameters['to_number'],
						'Message' => $parameters['sms_text'],
						'From' => $this->helper->getAdminField('mobilink_mask'),
						'Username' => $this->helper->getAdminField('mobilink_username'),
						'Password' =>  $this->encryptor->decrypt($this->helper->getAdminField('mobilink_password'))
				);
				break;
		}
		return $result;
	}
}