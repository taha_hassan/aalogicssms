<?php
namespace Aalogics\Sms\Model\Gateway\Mobilink\Api\Endpoints;
use Aalogics\Sms\Model\Gateway\Mobilink\Api\EndpointInterface;
use \Magento\Framework\DataObject;
use \Magento\Framework\Json\Helper\Data;

class Sendsms extends DataObject implements EndpointInterface {
	protected $_endpoint = 'sendsms_url.html';
	
	protected $logger;
	
	protected $scopeConfigObject;
	
	/**
	 *
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;
	
	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Aalogics\Sms\Logger\Logger $logger,
			\Magento\Framework\Json\Helper\Data $jsonHelper,
			array $data = []
	) {
		$this->jsonHelper = $jsonHelper;
		$this->logger = $logger;
		$this->scopeConfigObject = $scopeConfig;
		$data['endpoint'] = $this->_endpoint;
		parent::__construct($data);
	}
	
	public function makeRequestParams($parameters = []) {
		$params = array(
				'To' => $parameters['To'],
				'Message' => $parameters['Message'],
				'From' => $parameters['From'],
				'Username' => $parameters['Username'],
				'Password' => $parameters['Password']

		);
		
		//remove empty array keys
		foreach ($params as $key => $param) {
			if(!$param && strlen($param) == 0 ) {
				unset($params[$key]);
			}
		}
		return $params;
	}
	
	public function makeRequestHeaders($parameters = []) {
		return [
				'Authorization-Token' => $parameters['access_token'],
				'Accept' => '*/*',
				'Accept-Encoding' => 'gzip, deflate',
				'Source-Identifier' => $parameters['source_identifier'],
				'User-Agent' => 'runscope/0.1',
				'Content-Type' => 'application/json'
		];
		
	}
	
}