<?php
namespace Aalogics\Sms\Model\Gateway\Telenor\Api;

use \Magento\Framework\HTTP\ZendClientFactory;
use \Aalogics\Sms\Model\Gateway\Telenor\Api\Endpoints\Connect;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Aalogics\Sms\Logger\Logger;
use \Magento\Framework\HTTP\ZendClient;
use \Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use \Magento\Framework\Xml\Parser;
use \Aalogics\Sms\Helper\Data as SmsHelper;

class Client extends \Magento\Framework\DataObject {
	
	/**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfigObject;

    /**
     * 
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    protected $scopeConfigWriter;

    /**
     * @var \Aalogics\Sms\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory $clientFactory
     */
    protected $clientFactory;
    
    /**
     * 
     * @var \Aalogics\Sms\Model\Api\EndpointFactory
     */
    protected $endpointFactory;
    
     /**
      * @var \Aalogics\Sms\Helper\Data
      */
     protected $_helper;

     /**
      * @var \Aalogics\Sms\Helper\Data
      */
     protected $curlClient;  

    /**
     * 
     * @var \Magento\Framework\Xml\Parser
     */
    protected $xmlHelper;
	
	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Aalogics\Sms\Logger\Logger $logger,
			\Magento\Framework\HTTP\ZendClientFactory $clientFactory,
			EndpointFactory $endpointFactory,
			ConfigInterface $scopeWriter,
			\Magento\Framework\HTTP\Client\Curl $curl,
			\Aalogics\Sms\Helper\Data $SmsHelper,
			\Magento\Framework\Xml\Parser $xmlHelper,
			array $data = []
	) {
			$this->xmlHelper = $xmlHelper;
			$this->_helper = $SmsHelper;
			$this->scopeConfigWriter = $scopeWriter;
			$this->scopeConfigObject = $scopeConfig;
			$this->logger = $logger;
			$this->curlClient = $curl;
			$this->clientFactory = $clientFactory;		
			$this->endpointFactory = $endpointFactory;
			parent::__construct($data);
	}
	
	public function connect($username = null, $password = null, $sandbox = false, $forceCheck = false) {
	
		$oauth = $this->endpointFactory->create(\Aalogics\Sms\Model\Gateway\Telenor\Api\Endpoints\Connect::class);
		$url = $this->_helper->getAdminField( 'telenor_api_url' );

		
		if (!$username) {
			$username = $this->_helper->getAdminField( 'telenor_username');
		}
		if (!$password) {
			$password = $this->_helper->getAdminField( 'telenor_password');
		}
		
		/** @var \Magento\Framework\HTTP\ZendClient $client */
		$httpClient = $this->clientFactory->create();
		$clientConfig = ['verifypeer' => FALSE];
		$httpClient->setConfig($clientConfig);
		
		$token = $this->_helper->getAdminField( 'telenor_token');
		if(!$token || $forceCheck ) {
			$parameters = array(
				'username' => $username,
				'password' => $password,
			);

			$this->_helper->debug('token : '.$token);
			$this->_helper->debug('forceCheck : '.$forceCheck);
			$this->_helper->debug('client connect access token not found');
			$httpClient->setParameterGet($oauth->makeRequestParams($parameters));
			$this->scopeConfigWriter->saveConfig('aasms/general/telenor_token', FALSE, 'default', 0);
		}else{
			$this->_helper->debug('client connect access token found');
			$this->setData('access_token',$token);
			return $this;
		}
	
		try {
			$url = $url.$oauth->getData('endpoint');
			$httpClient->setUri($url);
// 			$httpClient->setMethod(ZendClient::GET);
			/* $httpClient->setHeaders($oauth->makeRequestHeaders(array(
				'source_identifier' => $this->_helper->getAdminField('synnexb2b/api_source_identifier')
			))); */
			$response = $httpClient->request()->getBody();
			if($response) {
				//save refresh token to file if not exists
				$response = $this->xmlHelper->loadXML($response)->xmlToArray();
				$this->_helper->debug('Client Connect Response',$response);
				if($response['corpsms']['data']) {
					$response['access_token'] = $response['corpsms']['data'];
					$response['response'] = $response['corpsms']['response'];
					unset($response['corpsms']);
				}
				$this->setData($response);
				if($this->getData('access_token')) {
					$this->_helper->debug('client connect setting access token');
					$this->scopeConfigWriter->saveConfig('aasms/general/telenor_token', $this->getData('access_token'),'default', 0);
				}
	
				// handle error
				if($this->getData('error')) {
					switch($this->getData('error')) {
						case 'invalid_token' :
							$this->scopeConfigWriter->saveConfig('aasms/general/telenor_token', FALSE,'default', 0);
							break;
						case 'invalid_client':
							$this->_helper->debug('client connect set error');
							$this->scopeConfigWriter->saveConfig('aasms/general/telenor_token', FALSE,'default', 0);
							break;
						default:
							//@todo write handling logic
							break;
					}
				}
					
			}
		} catch (\Exception $e) {
			$this->_helper->debug('Client Connect Request',$httpClient->getUri(TRUE));
			$this->scopeConfigWriter->saveConfig('aasms/general/telenor_token', FALSE,'default', 0);
		}
		return $this;
	}
	
	public function makeRequest($endPoint ,$method, $params , $headers = array()) {

		$url = $this->_helper->getAdminField( 'telenor_api_url' );
		$endPointObj = $this->endpointFactory->create($endPoint);
		$url = $url.$endPointObj->getData('endpoint');
		
		/* $headers = $endPointObj->makeRequestHeaders(array(
				'access_token' => $this->getData('access_token'),
				'source_identifier' => $this->scopeConfigObject->getValue('aaSms/synnexb2b/api_source_identifier')
		)); */
		
		/** @var \Magento\Framework\HTTP\ZendClient $client */
		$client = $this->clientFactory->create();
		$clientConfig = ['verifypeer' => FALSE];
		$client->setConfig($clientConfig);
		
		switch ($method) {
			case ZendClient::POST:
				$parameters = $endPointObj->makeRequestParams($params);
				$this->_helper->debug('Request Params',$this->jsonHelper->jsonEncode($parameters));
				$client->setRawData($this->jsonHelper->jsonEncode($parameters), 'application/json');
				if($endPointObj instanceof \Aalogics\Sms\Model\Api\Endpoints\TransactionDetails) {
					$url = $url.'/'.$params['id'];
				}
			break;
			case ZendClient::PUT:
				$parameters = $endPointObj->makeRequestParams($params);
				$this->_helper->debug('Request Params',$this->jsonHelper->jsonEncode($parameters));
				if($endPointObj instanceof \Aalogics\Sms\Model\Api\Endpoints\TransactionStatus) {
					$url = $url.'/'.$params['name'];
				}else {
					$url = $url.'/'.$params['id'];
				}
				$client->setRawData($this->jsonHelper->jsonEncode($parameters), 'application/json');
			break;
			case ZendClient::GET:
			case ZendClient::DELETE:
				$parameters = $endPointObj->makeRequestParams($params);
				$client->setParameterGet($parameters);
				
			break;
		}
		
		$this->_helper->debug('Request URL',$url);
// 		$this->_helper->debug('Request Headers',$headers);
		$this->_helper->debug('Request Method',$method);
		$this->_helper->debug('Request Params',$parameters);

		$client->setUri($url);
		$client->setMethod($method);
// 		$client->setHeaders($headers);
		try {
			$response = $client->request();
			$this->_helper->debug('Client Response',$response);
		} catch (\Exception $e) {
			$this->_helper->debug('Exception : ',$e->getMessage());
			$this->scopeConfigWriter->saveConfig('aasms/general/telenor_token', FALSE,'default', 0);
			throw new \Exception ( 'Exception : '.$e->getMessage() );
		}
		if (($response->getStatus() < 200 || $response->getStatus() > 210)) {
			$this->_helper->debug('Deployment marker request did not send a 200 status code.');
			throw new \Exception ( 'Deployment marker request did not send a 200 status code' );
		}
// 		$this->_helper->debug('Response',$response->getBody());
		$response = $this->xmlHelper->loadXML($response->getBody())->xmlToArray();
		return $response;
	}
}
