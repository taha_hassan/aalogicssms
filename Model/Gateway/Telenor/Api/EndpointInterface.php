<?php
namespace Aalogics\Sms\Model\Gateway\Telenor\Api;

interface EndpointInterface {
	
	public function makeRequestParams($parameters = []);
	
	public function makeRequestHeaders($parameters = []);
}