<?php
namespace Aalogics\Sms\Model\Gateway\Telenor\Api\Endpoints;
use Aalogics\Sms\Model\Gateway\Telenor\Api\EndpointInterface;
use \Magento\Framework\DataObject;
use \Aalogics\Sms\Logger\Logger;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class Connect extends DataObject implements EndpointInterface {

	protected $_endpoint = 'api/auth.jsp';
	
	protected $logger;
	
	protected $scopeConfigObject;
	
	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Aalogics\Sms\Logger\Logger $logger,
			array $data = []
	) {
		$this->logger = $logger;
		$this->scopeConfigObject = $scopeConfig;
		$data['endpoint'] = $this->_endpoint;
		parent::__construct($data);
	}
	
	public function makeRequestParams($parameters = []) {
		//use refresh token if exists
		if(
				$token = $this->scopeConfigObject->getValue('telenor_token')
				&& !isset($parameters['credentials']) && $parameters['credentials'] 
		) {
			$refresh_token = $this->scopeConfigObject->getValue('telenor_token');
			$params = array(
					'grant_type' => 'refresh_token',
					'refresh_token' => $refresh_token,
					'client_id' => $parameters['username'],
			);
		}else{
			$params = array(
// 					'grant_type' => 'client_credentials',
					'msisdn' => $parameters['username'],
					'password' => $parameters['password'],
			);
				
		}
		$this->logger->debug('Oaurth Params'.print_r($params, TRUE));
		return $params;
	}
	
	public function makeRequestHeaders($parameters = []) {
		$headers = array(
				'Content-Type' => 'application/x-www-form-urlencoded',
				'Accept' =>  '*/*',
				'Accept-Encoding' => 'gzip, deflate',
				'Content-Length' => '40',
				'Source-Identifier' => $parameters['source_identifier'],
				'User-Agent' => 'runscope/0.1'
		);
		$this->logger->debug('Oauth Headers'.print_r($headers, TRUE));
		return $headers;
		
	}
	
	public function checkToken() {
		$this->_endpoint = 'connect/introspect';
	}
	
}