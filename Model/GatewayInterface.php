<?php

namespace Aalogics\Sms\Model;

interface GatewayInterface
{
    public function sendOrderSms($parameters = []);

}