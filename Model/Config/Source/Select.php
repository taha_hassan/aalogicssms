<?php
/**
 * My own options
 *
 */

namespace Aalogics\Sms\Model\Config\Source;
class Select implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'twilio', 'label' => __('Twilio')],
            ['value' => 'telenor', 'label' => __('Telenor')],
            ['value' => 'mobilink', 'label' => __('Mobilink')],
            ['value' => 'ufone', 'label' => __('Ufone')]


        ];
    }
}
